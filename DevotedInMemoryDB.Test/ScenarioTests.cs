using Shouldly;
using Xunit;

namespace DevotedInMemoryDB.Test
{
    public class ScenarioTests
    {
        [Fact]
        public void scenario_one()
        {
            var db = new Database();
            db.Get("a").ShouldBe("NULL");
            db.Set("a", "foo");
            db.Set("b", "foo");
            db.Count("foo").ShouldBe(2);
            db.Count("bar").ShouldBe(0);
            db.Delete("a");
            db.Count("foo").ShouldBe(1);
            db.Set("b", "baz");
            db.Count("foo").ShouldBe(0);
            db.Get("b").ShouldBe("baz");
            db.Get("B").ShouldBe("NULL");
        }

        [Fact]
        public void scenario_two()
        {
            var db = new Database();
            db.Set("a", "foo");
            db.Set("a", "foo");
            db.Count("foo").ShouldBe(1);
            db.Get("a").ShouldBe("foo");
            db.Delete("a");
            db.Get("a").ShouldBe("NULL");
            db.Count("foo").ShouldBe(0);
        }

        [Fact]
        public void scenario_three()
        {
            var db = new Database();
            db.BeginTransaction();
            db.Set("a", "foo");
            db.Get("a").ShouldBe("foo");
            db.BeginTransaction();
            db.Set("a", "bar");
            db.Get("a").ShouldBe("bar");
            db.Set("a", "baz");
            db.RollbackTransaction();
            db.Get("a").ShouldBe("foo");
            db.RollbackTransaction();
            db.Get("a").ShouldBe("NULL");
        }

        [Fact]
        public void scenario_four()
        {
            var db = new Database();
            db.Set("a", "foo");
            db.Set("b", "baz");
            db.BeginTransaction();
            db.Get("a").ShouldBe("foo");
            db.Set("a", "bar");
            db.Count("bar").ShouldBe(1);
            db.BeginTransaction();
            db.Count("bar").ShouldBe(1);
            db.Delete("a");
            db.Get("a").ShouldBe("NULL");
            db.Count("bar").ShouldBe(0);
            db.RollbackTransaction();
            db.Get("a").ShouldBe("bar");
            db.Count("bar").ShouldBe(1);
            db.CommitTransactions();
            db.Get("a").ShouldBe("bar");
            db.Get("b").ShouldBe("baz");
        }
    }
}