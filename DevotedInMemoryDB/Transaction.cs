using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace DevotedInMemoryDB
{
    public class Transaction
    {
        private readonly ConcurrentDictionary<string, string> _db;
        private readonly IList<Action<ConcurrentDictionary<string, string>>> _commands;

        public Transaction(ConcurrentDictionary<string, string> db)
        {
            _db = db;
            _commands = new List<Action<ConcurrentDictionary<string, string>>>();
        }

        public void Commit()
        {
            foreach (var command in _commands)
            {
                command(_db);
            }
        }

        public ConcurrentDictionary<string, string> Rollback()
        {
            _commands.Clear();
            return _db;
        }
        
        public void Append(Action<ConcurrentDictionary<string, string>> action)
        {
            _commands.Add(action);
        }

        public ConcurrentDictionary<string, string> Apply()
        {
            var copy = new ConcurrentDictionary<string, string>(_db);
            foreach (var command in _commands)
            {
                command(copy);
            }

            return copy;
        }
    }
}