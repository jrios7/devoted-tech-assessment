using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace DevotedInMemoryDB
{
    public class Database
    {
        private ConcurrentDictionary<string, string> _db;
        private readonly Stack<Transaction> _transactions;

        public Database()
        {
            _db = new ConcurrentDictionary<string, string>();
            _transactions = new Stack<Transaction>();
        }

        public void Set(string key, string value)
        {
            var tx = HasTransactions() ? _transactions.Peek() : null;
            if (tx != null)
            {
                tx.Append(dict => dict.AddOrUpdate(key, value, (s, s1) => value));
            }
            else
            {
                _db.AddOrUpdate(key, value, (s, s1) => value);
            }
        }

        public void Delete(string key)
        {
            var tx = HasTransactions() ? _transactions.Peek() : null;
            if (tx != null)
            {
                tx.Append(dict => dict.TryRemove(key, out string value));
            }
            else
            {
                _db.TryRemove(key, out string value);
            }
        }

        public string Get(string key)
        {
            var tx = HasTransactions() ? _transactions.Peek() : null;
            if (tx != null)
            {
                var results = tx.Apply();
                return getOrNull(results, key);
            }

            return getOrNull(_db, key);
        }

        private string getOrNull(ConcurrentDictionary<string, string> db, string key)
        {
            db.TryGetValue(key, out string value);
            return value ?? "NULL";
        }

        public int Count(string value)
        {
            var tx = HasTransactions() ? _transactions.Peek() : null;
            if (tx != null)
            {
                var result= tx.Apply();
                return result.Values.Count(v => v == value);
            }

            return _db.Values.Count(v => v == value);
        }
        
        public void BeginTransaction()
        {
            if (HasTransactions())
            {
                var tx = _transactions.Peek();
                var copy = tx.Apply();
                var transaction = new Transaction(copy);
                _transactions.Push(transaction);
            }
            else
            {
                var transaction = new Transaction(_db);
                _transactions.Push(transaction);
            }
        }
        
        public void CommitTransactions()
        {
            foreach (var tx in _transactions)
            {
                tx.Commit();
            }
        }

        public void RollbackTransaction()
        {
            if (_transactions.Count == 0)
            {
                Console.WriteLine("TRANSACTION NOT FOUND");
            }
            else
            {
                var tx = _transactions.Pop();
                _db = tx.Rollback();
            }
        }
        
        private bool HasTransactions()
        {
            return _transactions.Count > 0;
        }
    }
}