﻿using System;

namespace DevotedInMemoryDB
{
    internal static class LineParser
    {
        public static Tuple<string, string, string> ParseLine(string line)
        {
            var parts = line.Split(' ');
            var param1 = parts.Length > 1 ? parts[1] : null;
            var param2 = parts.Length > 2 ? parts[2] : null;
            return new Tuple<string, string, string>(parts[0].ToUpper(), param1, param2);
        }
    }
    
    class Program
    {
        static void Main()
        {
            var db = new Database();

            while (true)
            {
                Console.Write(">> ");
                var line = Console.ReadLine();
                var (command, arg1, arg2) = LineParser.ParseLine(line);

                switch (command)
                {
                    case "SET":
                        db.Set(arg1, arg2);
                        break;
                    case "GET":
                        var value = db.Get(arg1);
                        Console.WriteLine(value);
                        break;
                    case "DELETE":
                        db.Delete(arg1);
                        break;
                    case "COUNT":
                        var count = db.Count(arg1);
                        Console.WriteLine(count);
                        break;
                    case "BEGIN":
                        db.BeginTransaction();
                        break;
                    case "COMMIT":
                        db.CommitTransactions();
                        break;
                    case "ROLLBACK":
                        db.RollbackTransaction();
                        break;
                    case "END":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Invalid operation. Expected one of [SET, GET, DELETE, COUNT, END, BEGIN, ROLLBACK, COMMIT]");
                        break;
                }
            }
        }
    }
}