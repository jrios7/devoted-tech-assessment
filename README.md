# Devoted Health In-Memory Database

This is a C# implementation of an in-memory database.

## Required Software

This solution uses the version 6.0 of the [dotnet sdk](https://dotnet.microsoft.com/en-us/download/dotnet) and is required to build and run the project.

## Instructions to Run

1. Ensure the dotnet cli tools are installed. See the link above.
2. Clone repository to local machine
3. Navigate to the folder where the code was cloned in a terminal
4. Copy and paste the following command: `dotnet run --project DevotedInMemoryDB/DevotedInMemoryDB.csproj`
5. Optionally, run the tests to see all four scenarios. From the root folder run: `dotnet test`